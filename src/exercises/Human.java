package exercises;

public class Human {

    int age;
    double weight;
    double height;
    String name;
    String sex;

    Human(String name, String sex, int age, double weight, double height)
    {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.height = height;
        this.sex = sex;
    }

    void getData()
    {
        System.out.println(name+" age is: "+age+"; weight is: "+weight+"; height is: "+height+"; sex is: "+sex);
    }

}
