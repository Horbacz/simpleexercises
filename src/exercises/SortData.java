package exercises;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

class SortData {
    public static void main(String[] args) {
        List<PersonData> personList = new LinkedList<>();
        personList.add(new PersonData("Artur", "Hak", 823453728123L));
        personList.add(new PersonData("Grzegorz", "Maja", 983236485921L));
        personList.add(new PersonData("Zygmunt", "Jaka", 234323543234L));
        personList.add(new PersonData("Kuba", "Zbad", 645832456328L));
        Collections.sort(personList);
        for(PersonData persons: personList)
        {
            System.out.println("Name: "+ persons.getName()+"; "+"Surname: "+persons.getSurname()+"; "+"Pesel: "+persons.getPesel());
        }
    }
}
class PersonData implements Comparable<PersonData>
{
    String name;
    String surname;
    long pesel;


    PersonData(String name, String surname, long pesel)
    {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
    }

    public String getName() {
        return name;
    }

    public long getPesel() {
        return pesel;
    }

    public String getSurname() {
        return surname;
    }

    @Override
    public int compareTo(PersonData o) {
        return this.name.compareTo(o.name);
    }
}

